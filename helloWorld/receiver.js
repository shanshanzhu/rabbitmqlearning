
var amqp = require('amqplib/callback_api');

amqp.connect('amqp://admin:syapse-rules@localhost', function(err, conn) {
  if (err) {
    return console.log(err)
  }
  conn.createChannel(function(err, ch) {
    var qName = 'helloxxx';
    // q must be the same name of sender
    ch.assertQueue(qName, {durable: false}, (err, q) => {
      if (err) console.log(err)
      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
      ch.consume(q.queue, (msg) => {
        console.log(" [x] Received %s", msg.content.toString())
      }, {noAck: true});
    });
  });
});