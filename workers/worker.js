const amqp = require('amqplib/callback_api')
var q = 'workers';
amqp.connect('amqp://admin:syapse-rules@localhost', (err, conn) => {
  if (err) {
    return console.log(err)
  }
  conn.createChannel(function(err, ch) {
    // q must be the same name of sender
    ch.assertQueue(q, {durable: true});
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
    ch.prefetch(1)
    ch.consume(q, (msg) => {
      // use the number of dots in msg for timeout seconds
      const msgStr = msg.content.toString()
      const i = msgStr.split('.').length - 1
      console.log(" [x] Received %s", msgStr)
      setTimeout(() => {
        console.log(" [x] done %s", msgStr)
        ch.ack(msg)
      }, i * 1000)
    })
  });
})