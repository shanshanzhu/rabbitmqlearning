const amqp = require('amqplib/callback_api')

const q = 'workers'
const testMsgs = ['first.', 'second......', 'third.', 'forth.']
amqp.connect('amqp://admin:syapse-rules@localhost', (err, conn) => {
  if (err) {
    return console.log(err)
  }
  conn.createChannel((err, ch) => {
    /*
    When RabbitMQ quits or crashes it will forget the queues and messages unless you tell it not to.
    Two things are required to make sure that messages aren't lost:
    - we need to mark both the queue and messages as durable. {durable: true}
      RabbitMQ doesn't allow you to redefine an existing queue with different parameters and
       will return an error to any program that tries to do that. use a different q name
    - {persistent: true}
     */
    ch.assertQueue(q, {durable: true})
    testMsgs.forEach((msg, i) => {
      let timeout = i
      // setTimeout(() => {
        ch.sendToQueue(q, new Buffer(`${msg}`), { persistent: true})
        console.log(`[x] sent ${msg}`)
      // }, i * 500)
    })
  })

})