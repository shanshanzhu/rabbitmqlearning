#!/usr/bin/env node

var amqp = require('amqplib/callback_api');
var EX_NAME = 'routing_rpc';
const routeName = process.argv[2] || 'test'
amqp.connect('amqp://admin:syapse-rules@localhost', function(err, conn) {
  if (err) {
    return console.log(err)
  }
  var msg = 'Hello World!';
  conn.createChannel(function(err, ch) {
    ch.assertExchange(EX_NAME, 'direct', {durable: false}, (err, ok) => {
      console.log(arguments)
      ch.publish(EX_NAME, routeName, new Buffer(msg));
      setTimeout(function() { conn.close(); process.exit(0) }, 500);
      // console.log(`[x] Sent "${msg}" to route "${routeName}" in exchange "${EX_NAME}"`);
    })
  })
});