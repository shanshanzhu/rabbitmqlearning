
var amqp = require('amqplib/callback_api');
var exName = 'routing';
const routeNames = process.argv.slice(2)
console.log(routeNames)
amqp.connect('amqp://admin:syapse-rules@localhost', function(err, conn) {
  if (err) {
    return console.log(err)
  }
  conn.createChannel(function(err, ch) {
    // q must be the same name of sender
    ch.assertExchange(exName, 'direct', {durable: false}, (err, {exchange}) => {
      if (err) {
        console.log(err)
        return
      }
      ch.assertQueue('', {exclusive: true}, (err, {queue}) => {
        console.log(" [*] Waiting for messages in queue %s. To exit press CTRL+C", queue);
        routeNames.forEach((route) => {
          ch.bindQueue(queue, exName, route)
          console.log(`binding queue ${queue} to route "${route}" from exchange "${exName}"`)
        })
        ch.consume(queue, (msg) => {
          // console.log(msg)
          console.log(`[x] Received ${msg.content.toString()} from route ${msg.fields.routingKey}`)
        }, {noAck: true})
      });
    })
  });
});