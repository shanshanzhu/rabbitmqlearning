#!/usr/bin/env node

var amqp = require('amqplib/callback_api');
var EX_NAME = 'routing';
const routeName = process.argv[2]
amqp.connect('amqp://admin:syapse-rules@localhost', function(err, conn) {
  if (err) {
    return console.log(err)
  }
  conn.createChannel(function(err, ch) {
    var msg = 'Hello World!';
    // ch.assertQueue(q, {durable: false});
    // Note: on Node 6 Buffer.from(msg) should be used
    ch.assertExchange(EX_NAME, 'direct', {durable: false})
    ch.publish(EX_NAME, routeName, new Buffer(msg));
    console.log(`[x] Sent "${msg}" to route "${routeName}" in exchange "${EX_NAME}"`);
  });
  setTimeout(function() { conn.close(); process.exit(0) }, 500);
});