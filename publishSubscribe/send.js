#!/usr/bin/env node

var amqp = require('amqplib/callback_api');
var exName = 'logs';

amqp.connect('amqp://admin:syapse-rules@localhost', function(err, conn) {
  if (err) {
    return console.log(err)
  }
  conn.createChannel(function(err, ch) {
    var msg = 'Hello World!';
    // ch.assertQueue(q, {durable: false});
    // Note: on Node 6 Buffer.from(msg) should be used
    ch.assertExchange(exName, 'fanout', {durable: false})
    ch.publish('logs','', new Buffer(msg));
    console.log(" [x] Sent %s", msg);
  });
  setTimeout(function() { conn.close(); process.exit(0) }, 500);
});