
var amqp = require('amqplib/callback_api');
var exName = 'logs';

amqp.connect('amqp://admin:syapse-rules@localhost', function(err, conn) {
  if (err) {
    return console.log(err)
  }
  conn.createChannel(function(err, ch) {
    // q must be the same name of sender
    ch.assertExchange(exName, 'fanout', {durable: false}, (err, {exchange}) => {
      if (err) {
        console.log(err)
        return
      }
      ch.assertQueue('', {exclusive: true}, (err, {queue}) => {
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
        ch.bindQueue(queue, exName, '')
        ch.consume(queue, (msg) => {
          console.log(" [x] Received %s", msg.content.toString())
        }, {noAck: true})
      });
    })
  });
});