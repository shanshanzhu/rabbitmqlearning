#Difference between worker and publish
1. workers, one buffer is sent to one queue. When multiple buffers are sent to 
multiple queues. It can be roundrobin or use `prefetch(1)` to send message to free queue
2. publish, one buffer is sent to multiple queues in one exchange(router)
bindQueue means subscribe queue to the exchange